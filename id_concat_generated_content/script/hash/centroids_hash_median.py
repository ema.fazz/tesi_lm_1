import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pprint
import json
from scipy.spatial.distance import hamming

"""

CENTROID
    transform the r bit string in the corrispondent number, calc the mean between the ids number and transfrom the mean in a bit num

    [001] [110] [111]
      1     6     7   = 14/3 = 4 = [100]

"""

def create_centroids_json(i, g, r_dim_csvs, save_json):

    centroids = {}

    
    for file in os.listdir(r_dim_csvs):

        if "hamming" not in file:

            csv_path=os.path.join(r_dim_csvs,file)
            print(csv_path)
            
            df = pd.read_csv(csv_path)

            final_r = df['Final-R']
            ids_to_num = []


            for el in final_r:
                bit_n = ""
                for j in el:
                    if j in ['0','1']:
                        bit_n += str(j)
                int_n = int(bit_n,2)
                ids_to_num.append(int_n)

            sorted = np.sort(ids_to_num)

            median_idx = int(len(sorted)/2)
            median = int(sorted[median_idx])

            getbinary = lambda x, n: format(x, 'b').zfill(n)

            centroid_node = getbinary(median, i)

            centroids[str(file)] = {
                "centroid":str(centroid_node),
                }

            print(centroid_node)
        

    with open(os.path.join(save_json,'centroids-hash-median.json'), 'w') as fp: # change result-hash with result-iscc to save the iscc results 
        json.dump(centroids, fp)


def calc_hamming_centroids(i, g, save_json):

    centroids = json.load(open(os.path.join(save_json,'centroids-hash-median.json'), 'r'))

    keys = centroids.keys()

    for k in keys:
        
        k_centr = centroids[k]['centroid']
        centroids[k]['hamming_cents'] = {}
        #print(k_centr)
        for kj in keys:
            if kj != k:
                kj_centr = centroids[kj]['centroid']
                
                print(list(k_centr), "   ", list(kj_centr))
                ham_k_kj = hamming(list(k_centr), list(kj_centr)) * len(list(k_centr))
                centroids[k]['hamming_cents'][kj] = ham_k_kj
        
    
    with open(os.path.join(save_json,'centroids-hash-median.json'), 'w') as fp: # change result-hash with result-iscc to save the iscc results 
        json.dump(centroids, fp)


def centroids_median(g):

    i = 256
    r_dim_csvs = "../../results/hash/csvs"

    save_json = "../../results/hash"

    

    create_centroids_json(i, g, r_dim_csvs, save_json)

    calc_hamming_centroids(i, g, save_json)