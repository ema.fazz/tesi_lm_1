from id_gen_iscc import *
from hamming_mean_iscc import *
from barplot_mean import *
from Centroids import Centroids as cds
from NewMetric import NewMetric as nm
from PlotCentroids import PlotCentroids as pc

g=1
r=256 # 16x16 concatenated r-bit strings
result_json = "../../results/iscc"
results_csvs = "../../results/iscc/csvs"

# gen id csvs
# main_id_concat_gen(g)
# gen hamming mean json for every img class
hamming_mean_concat(g)
#plot hamming means compared to the or generetaed once
barplot_concat_id()

# calc centroids to see if they are far apart each other
centroids = cds.centroids_median(r, results_csvs, result_json)


# calc the hamming distance between one centroids and all the others
hamming_mean_cents = cds.hamming_centroids_median(result_json)


# calc the new metric for concatenated id centroids
# # # nm.new_metric_cents_median(result_json # wrong method

# plot the centroids distances between the others and hamming matrix
pc.plot_centroids_median(result_json,os.path.join(result_json, "centroids_barplot"))
