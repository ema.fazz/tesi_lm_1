import os
import json
from pprint import pprint
import sys
import numpy as np

compare_json = json.load(open('compare_new_metric.json','r'))

id_generated_dir = compare_json.keys()

highlights_json = {}

min = sys.maxsize
max = -sys.maxsize -1

highlights_json['id_concat_gen'] = {}

for k in id_generated_dir:

    if "id_or" in k:

        highlights_json[k] = {}

        different_g = compare_json[k].keys()

        for g in different_g:
            min = sys.maxsize
            max = -sys.maxsize -1
            
            highlights_json[k][g] = {}

            different_r = compare_json[k][g].keys()

            r_min = {}
            r_max = {}
            highlights_json[k][g]['min'] = {}
            highlights_json[k][g]['max'] = {}

            for r in different_r:
                
                if compare_json[k][g][r]['compare_metric'] < min:
                    min = compare_json[k][g][r]['compare_metric']
                    nr_min = r
                    r_min = compare_json[k][g][r]
                
                if compare_json[k][g][r]['compare_metric'] > max:
                    max = compare_json[k][g][r]['compare_metric']
                    nr_max = r
                    r_max = compare_json[k][g][r]
                
            highlights_json[k][g]['min'][str(nr_min)] = r_min
            highlights_json[k][g]['max'][str(nr_max)] = r_max

    else:

        if compare_json[k]['compare_metric'] < min:
            min = compare_json[k]['compare_metric']
            nr_min = k
            r_min = compare_json[k]
        if compare_json[k]['compare_metric'] > max:
            max = compare_json[k]['compare_metric']
            nr_max = k
            r_max = compare_json[k]

        highlights_json['id_concat_gen']['min'] = {}
        highlights_json['id_concat_gen']['max'] = {}

        highlights_json['id_concat_gen']['min'][nr_min] = r_min
        highlights_json['id_concat_gen']['max'][nr_max] = r_max



id_keys = []
for ke in list(highlights_json['id_concat_gen'].keys()):
    id_keys.append(list(highlights_json['id_concat_gen'][ke].keys())[0])
print(id_keys)


compare_id_keys = ["id_concat_generated_meta","id_concat_generated_content","id_concat_generated_final"]

highlights_json['id_concat_gen']['med'] = {}
for i in compare_id_keys:
    if i not in id_keys:
        highlights_json['id_concat_gen']['med'][i] = compare_json[i]


with open('highlights.json', 'w') as fp: # change result-hash with result-iscc to save the iscc results 
            json.dump(highlights_json, fp)
