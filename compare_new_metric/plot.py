import sys
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import json
import numpy as np


if __name__ == "__main__":

    highl = json.load(open('highlights.json','r'))

    for k in highl.keys():

        if "id_or" in k:
            id_g_keys = highl[k].keys()
            fig = plt.figure(figsize=(12, 6))
            outer = gridspec.GridSpec(1, 2, wspace=0.2, hspace=0.2)
            #fig, axs = plt.subplots(1,2, figsize = (10,10))
            i = 1

            min_y = sys.maxsize
            max_y = -sys.maxsize-1

            for g_keys in id_g_keys:

                min = highl[k][g_keys]['min'][list(highl[k][g_keys]['min'].keys())[0]]['compare_metric']
                metric_min_iscc = highl[k][g_keys]['min'][list(highl[k][g_keys]['min'].keys())[0]]['iscc']['new_metric']
                metric_min_hash = highl[k][g_keys]['min'][list(highl[k][g_keys]['min'].keys())[0]]['hash']['new_metric']

                max = highl[k][g_keys]['max'][list(highl[k][g_keys]['max'].keys())[0]]['compare_metric']
                metric_max_iscc = highl[k][g_keys]['max'][list(highl[k][g_keys]['max'].keys())[0]]['iscc']['new_metric']
                metric_max_hash = highl[k][g_keys]['max'][list(highl[k][g_keys]['max'].keys())[0]]['hash']['new_metric']

                max_y_tmp = np.max([min,metric_min_iscc,metric_min_hash,max,metric_max_iscc,metric_max_hash]) + 0.5
                min_y_tmp = np.min([min,metric_min_iscc,metric_min_hash,max,metric_max_iscc,metric_max_hash]) - 0.5
                
                if min_y_tmp<min_y:
                    min_y = min_y_tmp
                if max_y_tmp>max_y:
                    max_y = max_y_tmp

                print(min_y, "   ", max_y)


            for g_keys in id_g_keys:

                min = highl[k][g_keys]['min'][list(highl[k][g_keys]['min'].keys())[0]]['compare_metric']
                metric_min_iscc = highl[k][g_keys]['min'][list(highl[k][g_keys]['min'].keys())[0]]['iscc']['new_metric']
                metric_min_hash = highl[k][g_keys]['min'][list(highl[k][g_keys]['min'].keys())[0]]['hash']['new_metric']

                max = highl[k][g_keys]['max'][list(highl[k][g_keys]['max'].keys())[0]]['compare_metric']
                metric_max_iscc = highl[k][g_keys]['max'][list(highl[k][g_keys]['max'].keys())[0]]['iscc']['new_metric']
                metric_max_hash = highl[k][g_keys]['max'][list(highl[k][g_keys]['max'].keys())[0]]['hash']['new_metric']

                r_min = list(highl[k][g_keys]['min'])[0]
                r_max = list(highl[k][g_keys]['max'])[0]

                x = np.arange(len([r_min,r_max]))
                width = 0.4
                ax = plt.subplot(1,2,i)
                
                rects1 = ax.bar(x - width/2, [metric_min_iscc, metric_max_iscc], width/2, label='Eff. Metric ISCC')
                rects2 = ax.bar(x, [metric_min_hash,metric_max_hash], width/2, label='Eff. Metric Hash')
                rects3 = ax.bar(x + width/2, [min, max], width/2, label='Compare Metrics')
                ax.set_xticks(x, [r_min, r_max])
                ax.set_title(g_keys)
                ax.axhline(y=0.0, color='r', linestyle='-')
                ax.legend()
                ax.set_ylim(min_y, max_y)
                
                fig.add_subplot(ax)

                i+=1
                fig.tight_layout()

            fig.savefig('./fig'+k+'.png')

        else: 

            id_keys = highl[k].keys()

            for ke in id_keys:

                for j in highl[k][ke].keys():

                    figure = plt.figure(figsize=(4,6))

                    key = highl[k][ke][j]['compare_metric']
                    key_iscc = highl[k][ke][j]['iscc']['new_metric']
                    key_hash = highl[k][ke][j]['hash']['new_metric']

                    labels = [j]
                    x = np.arange(len(labels))
                    width = 0.4
                    ax = plt.subplot()
                    
                    rects2 = ax.bar(x - width/2, [key_iscc], width/2, label='Eff. Metric ISCC')
                    rects2 = ax.bar(x, [key_hash], width/2, label='Eff. Metric Hash')
                    rects1 = ax.bar(x + width/2, [key], width/2, label='Compare Metrics')
                    ax.set_xticks(x, labels)
                    ax.set_title(j)
                    ax.axhline(y=0.0, color='r', linestyle='-')
                    ax.legend()
                    
                    figure.add_subplot(ax)

                    figure.tight_layout()

                    figure.savefig('./fig'+j+'.png')
                















