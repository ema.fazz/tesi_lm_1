import json
import os
import numpy as np
from scipy.spatial.distance import hamming
import pandas as pd


def new_metric_concat(centroid_json, csv_dir):

    result_json = json.load(open(centroid_json, 'r'))

    dictj = result_json

    img_cls = dictj.keys()
    hamming_dict = {}

    hamming_mean_cents_array = []
    avg_internal_dist_array = []
    new_metric_array = []

    for k in list(img_cls):
        
        """  MEAN DISTANCE FOR CENTROIDS  """
        hamming_dict = dictj[k]['hamming_cents']
        hamming_mean_cents = np.mean(list(hamming_dict.values()))

        """  MEAN DISTANCE FROM CLASS CENTROID TO THE IMAGES  """
        k_csv = csv_dir+"/"+k
        df = pd.read_csv(k_csv)

        final_r = df['Final-R']
        k_centr = dictj[k]['centroid']
        sum_hamming = 0
        for el in final_r: 

            el1 = str(el).replace(' ','').replace('[','').replace(']','').replace(',','')
            sum_hamming += hamming(list(k_centr), list(el1)) * len(list(k_centr))
        
        avg_internal_dist = sum_hamming / len(final_r)
        hamming_mean_cents_array.append(hamming_mean_cents)
        avg_internal_dist_array.append(avg_internal_dist)
        new_metric_array.append(hamming_mean_cents - avg_internal_dist)

    
    hamming_mean_cents = np.mean(hamming_mean_cents_array)
    hamming_sd_cents = np.std(hamming_mean_cents_array)
    avg_internal_dist = np.mean(avg_internal_dist_array)
    std_internal_dist = np.std(avg_internal_dist_array)
    new_metric = np.mean(new_metric_array)

    return hamming_mean_cents, hamming_sd_cents, avg_internal_dist, std_internal_dist, new_metric

def new_metric_or(r,g,rootdir_cents,csv_dir,check_iscc):

    if check_iscc == "iscc":

        centroid_json = os.path.join(rootdir_cents, "centroids-median-iscc.json")

    else:
        g = g*4
        centroid_json = os.path.join(rootdir_cents, "centroids-hash-median.json")

    r_dir = "r-"+str(r)

    g_dir = "g-"+str(g)

    result_json = json.load(open(centroid_json, 'r'))

    dictj = result_json[r_dir][g_dir]

    img_cls = dictj.keys()
    hamming_dict = {}

    hamming_mean_cents_array = []
    avg_internal_dist_array = []
    new_metric_array = []

    for k in list(img_cls):
        
        """  MEAN DISTANCE FOR CENTROIDS  """
        hamming_dict = dictj[k]['hamming_cents']
        hamming_mean_cents = np.mean(list(hamming_dict.values()))

        """  MEAN DISTANCE FROM CLASS CENTROID TO THE IMAGES  """
        if check_iscc == "iscc":
            k_csv = csv_dir+"r-"+str(r)+"/g-"+str(g)+"/iscc-csvs/"+k
        else:
            k_csv = csv_dir+"r-"+str(r)+"/g-"+str(g)+"/hash-csvs/"+k
        
        df = pd.read_csv(k_csv)
        final_r = df['Final-R']
        k_centr = dictj[k]['centroid']
        sum_hamming = 0
        
        for el in final_r:
            
            el1 = str(el).replace(' ','').replace('[','').replace(']','').replace(',','')
            sum_hamming += hamming(list(k_centr), list(el1)) * len(list(k_centr))
        
        avg_internal_dist = sum_hamming / len(final_r)
        hamming_mean_cents_array.append(hamming_mean_cents)
        avg_internal_dist_array.append(avg_internal_dist)
        new_metric_array.append(hamming_mean_cents - avg_internal_dist)

    
    hamming_mean_cents = np.mean(hamming_mean_cents_array)
    hamming_sd_cents = np.std(hamming_mean_cents_array)
    avg_internal_dist = np.mean(avg_internal_dist_array)
    std_internal_dist = np.std(avg_internal_dist_array)
    new_metric = np.mean(new_metric_array)

    return hamming_mean_cents, hamming_sd_cents, avg_internal_dist, std_internal_dist, new_metric

def compare_new_metric():

    root = ".."
    final_compare_json = {}

    for dir in os.listdir(root):
        
        if "id_concat" in dir:

            centroids_json_iscc = root+"/"+dir+"/results/iscc/centroids-median-iscc.json"
            centroids_json_hash = root+"/"+dir+"/results/hash/centroids-hash-median.json"
            csvs_dir_iscc = root+"/"+dir+"/results/iscc/csvs"
            csvs_dir_hash = root+"/"+dir+"/results/hash/csvs"

            hamming_mean_cents_hash, hamming_sd_cents_hash, avg_internal_dist_hash, std_internal_dist_hash, new_metric_hash  = new_metric_concat(centroids_json_hash, csvs_dir_hash)
            hamming_mean_cents_iscc, hamming_sd_cents_iscc, avg_internal_dist_iscc, std_internal_dist_iscc, new_metric_iscc  = new_metric_concat(centroids_json_iscc, csvs_dir_iscc)
            

            compare_metric = new_metric_iscc - abs(new_metric_hash)

            final_compare_json[dir] = {}

            final_compare_json[dir]['iscc']={}
            final_compare_json[dir]['iscc']['classes_mean_distance'] = hamming_mean_cents_iscc
            final_compare_json[dir]['iscc']['classes_std_distance'] = hamming_sd_cents_iscc
            final_compare_json[dir]['iscc']['mean_internal_distance'] = avg_internal_dist_iscc
            final_compare_json[dir]['iscc']['std_internal_distance'] = std_internal_dist_iscc
            final_compare_json[dir]['iscc']['new_metric'] = new_metric_iscc

            final_compare_json[dir]['hash']={}
            final_compare_json[dir]['hash']['classes_mean_distance'] = hamming_mean_cents_hash
            final_compare_json[dir]['hash']['classes_std_distance'] = hamming_sd_cents_hash
            final_compare_json[dir]['hash']['mean_internal_distance'] = avg_internal_dist_hash
            final_compare_json[dir]['hash']['std_internal_distance'] = std_internal_dist_hash
            final_compare_json[dir]['hash']['new_metric'] = new_metric_hash

            final_compare_json[dir]['compare_metric'] = compare_metric
        
        elif "id_or" in dir:

            final_compare_json[dir] = {}

            for g in [2,4]:

                centroids_dir = root+"/"+dir+"/results/iscc-"+str(g)+"_hash-"+str(g*4)+"/centroids"
                csv_dir = root+"/"+dir+"/results/iscc-"+str(g)+"_hash-"+str(g*4)+"/csvs/"

                final_compare_json[dir]["iscc-"+str(g)+"_hash-"+str(g*4)] = {}

                r = 8

                while r<=1024:

                    final_compare_json[dir]["iscc-"+str(g)+"_hash-"+str(g*4)]['r-'+str(r)] = {}

                    hamming_mean_cents_hash, hamming_sd_cents_hash, avg_internal_dist_hash, std_internal_dist_hash, new_metric_hash  = new_metric_or(r,g,centroids_dir, csv_dir, 'hash')
                    hamming_mean_cents_iscc, hamming_sd_cents_iscc, avg_internal_dist_iscc, std_internal_dist_iscc, new_metric_iscc  = new_metric_or(r,g,centroids_dir, csv_dir, 'iscc')
                    
                    compare_metric = new_metric_iscc - abs(new_metric_hash)

                    final_compare_json[dir]["iscc-"+str(g)+"_hash-"+str(g*4)]['r-'+str(r)]['iscc']={}
                    final_compare_json[dir]["iscc-"+str(g)+"_hash-"+str(g*4)]['r-'+str(r)]['iscc']['classes_mean_distance'] = hamming_mean_cents_iscc
                    final_compare_json[dir]["iscc-"+str(g)+"_hash-"+str(g*4)]['r-'+str(r)]['iscc']['classes_std_distance'] = hamming_sd_cents_iscc
                    final_compare_json[dir]["iscc-"+str(g)+"_hash-"+str(g*4)]['r-'+str(r)]['iscc']['mean_internal_distance'] = avg_internal_dist_iscc
                    final_compare_json[dir]["iscc-"+str(g)+"_hash-"+str(g*4)]['r-'+str(r)]['iscc']['std_internal_distance'] = std_internal_dist_iscc
                    final_compare_json[dir]["iscc-"+str(g)+"_hash-"+str(g*4)]['r-'+str(r)]['iscc']['new_metric'] = new_metric_iscc

                    final_compare_json[dir]["iscc-"+str(g)+"_hash-"+str(g*4)]['r-'+str(r)]['hash']={}
                    final_compare_json[dir]["iscc-"+str(g)+"_hash-"+str(g*4)]['r-'+str(r)]['hash']['classes_mean_distance'] = hamming_mean_cents_hash
                    final_compare_json[dir]["iscc-"+str(g)+"_hash-"+str(g*4)]['r-'+str(r)]['hash']['classes_std_distance'] = hamming_sd_cents_hash
                    final_compare_json[dir]["iscc-"+str(g)+"_hash-"+str(g*4)]['r-'+str(r)]['hash']['mean_internal_distance'] = avg_internal_dist_hash
                    final_compare_json[dir]["iscc-"+str(g)+"_hash-"+str(g*4)]['r-'+str(r)]['hash']['std_internal_distance'] = std_internal_dist_hash
                    final_compare_json[dir]["iscc-"+str(g)+"_hash-"+str(g*4)]['r-'+str(r)]['hash']['new_metric'] = new_metric_hash

                    final_compare_json[dir]["iscc-"+str(g)+"_hash-"+str(g*4)]['r-'+str(r)]['compare_metric'] = compare_metric
            
                    r *= 2

            

    with open(os.path.join('./','compare_new_metric.json'), 'w') as fp:
        json.dump(final_compare_json, fp)


if __name__ == "__main__":

    compare_new_metric()



