import sys
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import json
import numpy as np


if __name__ == "__main__":

    highl = json.load(open('plot-peech.json','r'))

    for k in highl.keys():

        if "Non" in k:
            id_g_keys = highl[k].keys()
            fig, ax1 = plt.subplots()
            
            #fig, axs = plt.subplots(1,2, figsize = (10,10))
            i = 1

            min = highl[k][list(id_g_keys)[0]]['min'][list(highl[k][list(id_g_keys)[0]]['min'].keys())[0]]['compare_metric']
            metric_min_iscc = highl[k][list(id_g_keys)[0]]['min'][list(highl[k][list(id_g_keys)[0]]['min'].keys())[0]]['iscc']['new_metric']
            metric_min_hash = highl[k][list(id_g_keys)[0]]['min'][list(highl[k][list(id_g_keys)[0]]['min'].keys())[0]]['hash']['new_metric']

            max = highl[k][list(id_g_keys)[1]]['max'][list(highl[k][list(id_g_keys)[1]]['max'].keys())[0]]['compare_metric']
            metric_max_iscc = highl[k][list(id_g_keys)[1]]['max'][list(highl[k][list(id_g_keys)[1]]['max'].keys())[0]]['iscc']['new_metric']
            metric_max_hash = highl[k][list(id_g_keys)[1]]['max'][list(highl[k][list(id_g_keys)[1]]['max'].keys())[0]]['hash']['new_metric']
            
            med = highl[k][list(id_g_keys)[2]]['max'][list(highl[k][list(id_g_keys)[2]]['max'].keys())[0]]['compare_metric']
            metric_med_iscc = highl[k][list(id_g_keys)[2]]['max'][list(highl[k][list(id_g_keys)[2]]['max'].keys())[0]]['iscc']['new_metric']
            metric_med_hash = highl[k][list(id_g_keys)[2]]['max'][list(highl[k][list(id_g_keys)[2]]['max'].keys())[0]]['hash']['new_metric']

            min_y = min-1
            max_y = metric_max_iscc+1

            rs = [
                str(list(highl[k][list(id_g_keys)[0]]['min'].keys())[0]) + '\n Content',
                str(list(highl[k][list(id_g_keys)[1]]['max'].keys())[0]) + '\n Meta & Con.',
                str(list(highl[k][list(id_g_keys)[2]]['max'].keys())[0]) + '\n Meta'
            ]

            x = np.arange(len(rs))
            width = 0.4
            rects1 = ax1.bar(x - width/2, [metric_min_iscc, metric_max_iscc, metric_med_iscc], width/2, label='Eff. Metric ISCC')
            for i, v in enumerate([metric_min_iscc, metric_max_iscc, metric_med_iscc]):
                ax1.text(i - .29, v+.1, str(v)[:4], color='black')
            rects2 = ax1.bar(x, [metric_min_hash,metric_max_hash,metric_med_hash], width/2, label='Eff. Metric Hash')
            for i, v in enumerate([metric_min_hash,metric_max_hash,metric_med_hash]):
                ax1.text(i-0.1, v+.1, str(v)[:4], color='black')
            rects3 = ax1.bar(x + width/2, [min, max, med], width/2, label='Compare Metrics')
            for i, v in enumerate([min, max, med]):
                if i == 0:
                    ax1.text(i + .1, v-.3, str(v)[:4], color='black')
                else:
                    ax1.text(i + .1, v+.1, str(v)[:4], color='black')
            ax1.set_xticks(x, rs)
            ax1.set_title(k+' con g=2')
            ax1.axhline(y=0.0, color='r', linestyle='-')
            ax1.legend()
            ax1.set_ylim(min_y, max_y)
            
            fig.savefig('./fig'+k+'.png')

        else:

            id_g_keys = highl[k].keys()
            fig, ax1 = plt.subplots()
            
            #fig, axs = plt.subplots(1,2, figsize = (10,10))
            i = 1

            min = highl[k][list(id_g_keys)[0]]['min']['compare_metric']
            metric_min_iscc = highl[k][list(id_g_keys)[0]]['min']['iscc']['new_metric']
            metric_min_hash = highl[k][list(id_g_keys)[0]]['min']['hash']['new_metric']

            max = highl[k][list(id_g_keys)[1]]['max']['compare_metric']
            metric_max_iscc = highl[k][list(id_g_keys)[1]]['max']['iscc']['new_metric']
            metric_max_hash = highl[k][list(id_g_keys)[1]]['max']['hash']['new_metric']
            
            med = highl[k][list(id_g_keys)[2]]['max']['compare_metric']
            metric_med_iscc = highl[k][list(id_g_keys)[2]]['max']['iscc']['new_metric']
            metric_med_hash = highl[k][list(id_g_keys)[2]]['max']['hash']['new_metric']

            min_y = min-1
            max_y = metric_max_iscc+1

            rs = [
                'Content',
                'Meta & Con.',
                'Meta'
            ]

            x = np.arange(len(rs))
            width = 0.4
            rects1 = ax1.bar(x - width/2, [metric_min_iscc, metric_max_iscc, metric_med_iscc], width/2, label='Eff. Metric ISCC')
            for i, v in enumerate([metric_min_iscc, metric_max_iscc, metric_med_iscc]):
                ax1.text(i - .29, v+.1, str(v)[:4], color='black')
            rects2 = ax1.bar(x, [metric_min_hash,metric_max_hash,metric_med_hash], width/2, label='Eff. Metric Hash')
            for i, v in enumerate([metric_min_hash,metric_max_hash,metric_med_hash]):
                ax1.text(i-0.1, v+.1, str(v)[:4], color='black')
            rects3 = ax1.bar(x + width/2, [min, max, med], width/2, label='Compare Metrics')
            for i, v in enumerate([min, max, med]):
                ax1.text(i + .1, v+.1, str(v)[:4], color='black')
            ax1.set_xticks(x, rs)
            ax1.set_title(k)
            ax1.axhline(y=0.0, color='r', linestyle='-')
            ax1.legend()
            ax1.set_ylim(min_y, max_y)
            

            fig.savefig('./fig'+k+'.png')

        # else: 

        #     id_keys = highl[k].keys()

        #     for ke in id_keys:

        #         for j in highl[k][ke].keys():

        #             figure = plt.figure(figsize=(4,6))

        #             key = highl[k][ke][j]['compare_metric']
        #             key_iscc = highl[k][ke][j]['iscc']['new_metric']
        #             key_hash = highl[k][ke][j]['hash']['new_metric']

        #             labels = [j]
        #             x = np.arange(len(labels))
        #             width = 0.4
        #             ax = plt.subplot()
                    
        #             rects2 = ax.bar(x - width/2, [key_iscc], width/2, label='Eff. Metric ISCC')
        #             rects2 = ax.bar(x, [key_hash], width/2, label='Eff. Metric Hash')
        #             rects1 = ax.bar(x + width/2, [key], width/2, label='Compare Metrics')
        #             ax.set_xticks(x, labels)
        #             ax.set_title(j)
        #             ax.axhline(y=0.0, color='r', linestyle='-')
        #             ax.legend()
                    
        #             figure.add_subplot(ax)

        #             figure.tight_layout()

        #             figure.savefig('./fig'+j+'.png')
                















