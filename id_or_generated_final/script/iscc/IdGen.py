from binascii import hexlify
from operator import index, xor
import iscc_sdk as idk
import jcs
import hashlib as hl
from data_url import DataURL
import os
import base64
import random
import pandas as pd
from scipy.spatial.distance import hamming
from openlocationcode import openlocationcode as olc

class IdGen():

    def calcISCC( path, title, metas):

        meta_serialized = jcs.canonicalize(metas)
        

        du_obj = DataURL.from_data('application/json',base64_encode=True, data=meta_serialized)

        ic_meta = idk.IsccMeta(name=title,description="description",meta=du_obj.url) 
        iscc_meta = idk.embed_metadata(path,ic_meta)

        #iscc_meta1 = idk.code_meta(iscc_meta)

        iscc_meta = idk.code_meta(iscc_meta).dict()
        
        base64_metas = iscc_meta['meta'][iscc_meta['meta'].index(',')+1:]

        
        decoded_metas = base64.b64decode(base64_metas).decode('ascii')
        print("base64 decoded metadata: "+decoded_metas)
        print()

        iscc_content = idk.code_content(path).dict() 

        print("metadata of the image: ",iscc_meta)
        print()
        print("content iscc of the image: ",iscc_content)
        print()

        return iscc_meta['iscc'][5:], iscc_content['iscc'][5:]


    # Find the node into the Hypercube where assign the file



    def calc_node_id(iscc_meta, iscc_content, r, g):

        # check that g is between [1,15]
        if g > 15:
            g = 15
        elif g < 1:
            g = 1

        iscc_decomposed_meta = []
        iscc_decomposed_content = []
        tmp_str_meta = ""
        tmp_str_content = ""

        for i in range(0,len(iscc_meta)): # meta and content have the same length

            # create the decomposed iscc string to calculate the hash and then the node ID 
            tmp_str_meta += iscc_meta[i]  
            tmp_str_content += iscc_content[i]

            if (i+1) % g == 0:
                iscc_decomposed_meta.append(tmp_str_meta)
                iscc_decomposed_content.append(tmp_str_content)
                tmp_str_meta = ""
                tmp_str_content = ""
            
            # if r-bit string is odd then it have to append the last char to the last decomposed iscc string
            if g % 2 != 0 and i == len(iscc_meta)-1:
                iscc_decomposed_meta[len(iscc_decomposed_meta)-1] += iscc_meta[i]
                iscc_decomposed_content[len(iscc_decomposed_content)-1] += iscc_content[i]

        #print(iscc_decomposed)

        # Calc the node ID where the file will be saved
        r_bit_posisitons_meta = []
        r_bit_posisitons_content = []

        for i in range(0, len(iscc_decomposed_meta)): # meta and content have the same length
            
            deco_hex_meta = hexlify(iscc_decomposed_meta[i].encode()).decode()
            deco_hex_content = hexlify(iscc_decomposed_content[i].encode()).decode()
            val_int_meta = int(deco_hex_meta,16) # base 16 for hexa string
            val_int_content = int(deco_hex_content,16) # base 16 for hexa string
            mod_meta = val_int_meta % r
            mod_content = val_int_content % r
            r_bit_posisitons_meta.append(mod_meta)
            r_bit_posisitons_content.append(mod_content)

        print(r_bit_posisitons_content)
        print(r_bit_posisitons_meta)
        r_string_meta = [0 for i in range(0,r)]
        r_string_content = [0 for i in range(0,r)]

        for el in r_bit_posisitons_meta:
            r_string_meta[el] = 1
        
        for el in r_bit_posisitons_content:
            r_string_content[el] = 1

        final_r = [0 for i in range(0,r)]

        for i in range(0,r):
            bool_m = bool(r_string_meta[i])
            bool_c = bool(r_string_content[i])
            if bool_c or bool_m: #(bool_c and not bool_m) or (not bool_c and bool_m):
                final_r[i] = 1

        return r_string_meta, r_string_content, final_r


    # Create a csv file for every folder with simil images inside and save them into the "csvs" folder 
    def create_csv(self, rootdir, r, csv_folder, g):
        
        for root, subFolders, files in os.walk(rootdir):
            
            for dir in subFolders:

                i = 0
                dir_path = os.path.join(rootdir, dir)
                dir_df = pd.DataFrame(columns = ['Img-Name','Meta','R-Meta','Content', 'R-Content', 'Final-R'])

                # per le immagini con lo stesso soggetto inserisco la stessa posizione
                latlong = {
                            "lat": round(random.uniform(-190,190), 6),
                            "long": round(random.uniform(-190,190), 6),
                        }    

                _olc = olc.encode(latlong['lat'], latlong['long'], 8)
                meta = {
                        "@type":"Places",
                        "olc": _olc
                    }
                
                print(meta)

                for file in os.listdir(dir_path):

                    file_path = os.path.join(dir_path, file)
                    file_title = "Img " + str(file)
                    print(file_path)

                    iscc_meta, iscc_content = self.calcISCC(file_path,file_title,meta)
                    r_string_m, r_string_c, final_r = self.calc_node_id(iscc_meta, iscc_content, r, g)
                    
                    print(r_string_m, "    ", r_string_c, "    ", final_r)
                    print()

                    file_codes = [str(file),iscc_meta,str(r_string_m),iscc_content,str(r_string_c),str(final_r)]

                    dir_df.loc[i] = file_codes
                    i += 1
                print(dir_df)
                dir_df.to_csv(csv_folder+"/"+str(dir)+".csv", index=False)


    # Calc hamming distance between codes and create distance matrices
    def code_hamming_distance(self, csvs, hamming_csvs):

        for file in os.listdir(csvs):

            if "hamming" not in str(file):

                hamming_file = str(file)[:-4] + "_hamming.csv"

                df_codes = pd.read_csv(os.path.join(csvs, file))
                idx_h_df = list(df_codes['Img-Name'])
                idx_h_df.insert(0,'Type')
                cols = idx_h_df[1:]
                df_codes = df_codes.set_index('Img-Name')

                concat_df = pd.DataFrame()

                for col in df_codes.keys():

                    sep = [col for i in range(0,len(cols))]

                    h_df = pd.DataFrame(columns=cols, index=idx_h_df)
                    h_df.loc['Type'] = sep         

                    for idx, row in df_codes.iterrows():
                            img = idx
                            str1 = str(df_codes.loc[img,col]).replace(' ','').replace('[','').replace(']','').replace(',','')

                            for idx, row in df_codes.iterrows():
                                
                                str2=str(df_codes.loc[idx,col]).replace(' ','').replace('[','').replace(']','').replace(',','')
                                # print(str1, "    ", str2)
                                # hamming(x, y) * len(x) -> * len(x) because hamming() returns the percentage of elements that differs
                                h_dist = hamming(list(str1),list(str2)) * len(list(str1))
                                # print(h_dist)
                                h_df.loc[img,idx] = round(h_dist)

                    concat_df = pd.concat([concat_df,h_df], ignore_index=False)

                print(concat_df)
                concat_df.to_csv(os.path.join(hamming_csvs, hamming_file))


    
    

    



        








