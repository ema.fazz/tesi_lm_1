
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pprint
import json


def hamming_mean_hash(g):

    i = 8
    # r_dim_csvs = "../../results/iscc-2_hash-8"
    r_dim_csvs = "../../results/iscc-"+str(int(g/4))+"_hash-"+str(g)+"/csvs"
    

    hamming_mean_dict = {}

    while i<=1024:

        R = i # number of bits into the bit string
        

        r_dir = "r-"+str(i)
        new_r_dir = os.path.join(r_dim_csvs, r_dir)

        hamming_mean_dict[r_dir] = {}

        g_dir = "g-"+str(g)
        g_path = os.path.join(new_r_dir, g_dir)
        hamming_mean_dict[r_dir][g_dir] = {}

        hash_dir = "hash-csvs"
        hash_path = os.path.join(g_path, hash_dir)

        iscc_dir = "iscc-csvs"
        iscc_path = os.path.join(g_path, iscc_dir)

        for file in os.listdir(hash_path): 
            
            if "hamming" in str(file):

                csv = os.path.join(hash_path, file) # 

                hamming_mean_dict[r_dir][g_dir][str(file)] = {}
                df = pd.read_csv(csv) 
                j = 0

                for idx, row in df.iterrows():

                    if idx % len(df.keys()) == 0:

                        tmp_df = pd.DataFrame(columns=df.keys())
                        arr = df[idx:(idx+len(df.keys()))][:]
                        tmp_df = pd.DataFrame(arr)
                        tmp_df = tmp_df.set_index('Unnamed: 0')
                        code = tmp_df.loc['Type'][0] # take the type of the code 
                        tmp_df = tmp_df.drop('Type',axis=0) # drop the type given that is write in the title
                        tmp_df = tmp_df.astype(np.float64) # cast all element to float 
                        
                        #tmp_df = tmp_df.mask(np.equal(*np.indices(tmp_df.shape))) # mask the diagonal
                        df_lower = np.tril(tmp_df)
                        df_lower[df_lower == 0] = np.nan
                        mean = np.nanmean(df_lower)
                        std = np.nanstd(df_lower)
                        hamming_mean_dict[r_dir][g_dir][str(file)][code+"-mean"] = mean
                        hamming_mean_dict[r_dir][g_dir][str(file)][code+"-std"] = std

            # pprint.pprint(hamming_mean_dict)

            result = "../../results/iscc-"+str(int(g/4))+"_hash-"+str(g)+"/hamming_mean/hamming-mean-hash.json"
            with open(result, 'w') as fp: 
                json.dump(hamming_mean_dict, fp)


        i = i*2