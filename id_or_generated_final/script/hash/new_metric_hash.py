import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pprint
import json
from scipy.spatial.distance import hamming


"""

results = []
for each class i
   
    distanza_da_altre_classi = []
    for each class j != i
        distanza_da_altre_classi.append( hamming(centroide(i),centroide(j)) )
   
    distanza_interna = []
    for each image k != centroide(i)
        distanza_interna.append( hamming(centroide(i), k) )

    results.append( avg(distanza_da_altre_classi) - avg(distanza_interna) )

metrica_finale = avg(results)

"""

def new_metric_cents_mean(r,g,rootdir_cents):

    centroid_json = os.path.join(rootdir_cents, "centroids-hash.json")

    result_json = json.load(open(centroid_json, 'r'))

    new_metric_json = {}

    r=8

    while r<=1024:

        r_dir = "r-"+str(r)

        g_dir = "g-"+str(g)

        dictj = result_json[r_dir][g_dir]

        img_cls = dictj.keys()
        hamming_dict = {}


        new_metric = 0

        for k in list(img_cls):
            
            """  MEAN DISTANCE FOR CENTROIDS  """
            hamming_dict = dictj[k]['hamming_cents']
            hamming_mean_cents = np.mean(list(hamming_dict.values()))

            """  MEAN DISTANCE FROM CLASS CENTROID TO THE IMAGES  """
            k_csv = "../../results/iscc-"+str(int(g/4))+"_hash-"+str(g)+"/csvs/"+r_dir+"/"+g_dir+""+"/hash-csvs/"+k
            df = pd.read_csv(k_csv)

            final_r = df['Final-R']
            k_centr = dictj[k]['centroid']
            sum_hamming = 0
            for el in final_r:
                
                el = [str(i) for i in k_centr]
                #print(el, k_centr)
                sum_hamming += hamming(list(k_centr), list(el)) * len(list(k_centr))
            
            avg_internal_dist = sum_hamming / len(final_r)
            #print(k)
            new_metric += (hamming_mean_cents - avg_internal_dist)
        
        new_metric = new_metric/len(img_cls)
        print(new_metric)
        new_metric_json[r_dir] = new_metric

        r *= 2

    with open("../../results/iscc-"+str(int(g/4))+"_hash-"+str(g)+"/new_metric/new_metric_hash_mean.json", 'w') as fp: 
        json.dump(new_metric_json, fp)

def new_metric_cents_median(r,g,rootdir_cents):

    centroid_json = os.path.join(rootdir_cents, "centroids-hash-median.json")

    result_json = json.load(open(centroid_json, 'r'))

    new_metric_json = {}

    r=8

    while r<=1024:

        r_dir = "r-"+str(r)

        g_dir = "g-"+str(g)

        dictj = result_json[r_dir][g_dir]

        img_cls = dictj.keys()
        hamming_dict = {}


        new_metric = 0

        for k in list(img_cls):
            
            """  MEAN DISTANCE FOR CENTROIDS  """
            hamming_dict = dictj[k]['hamming_cents']
            hamming_mean_cents = np.mean(list(hamming_dict.values()))

            """  MEAN DISTANCE FROM CLASS CENTROID TO THE IMAGES  """
            k_csv = "../../results/iscc-"+str(int(g/4))+"_hash-"+str(g)+"/csvs/"+r_dir+"/"+g_dir+""+"/hash-csvs/"+k
            df = pd.read_csv(k_csv)

            final_r = df['Final-R']
            k_centr = dictj[k]['centroid']
            sum_hamming = 0
            for el in final_r:
                
                el = [str(i) for i in k_centr]
                #print(el, k_centr)
                sum_hamming += hamming(list(k_centr), list(el)) * len(list(k_centr))
            
            avg_internal_dist = sum_hamming / len(final_r)
            #print(k)
            new_metric += (hamming_mean_cents - avg_internal_dist)
        
        new_metric = new_metric/len(img_cls)
        print(new_metric)
        
        new_metric_json[r_dir] = new_metric

        r *= 2

    with open("../../results/iscc-"+str(int(g/4))+"_hash-"+str(g)+"/new_metric/new_metric_hash_median.json", 'w') as fp: 
        json.dump(new_metric_json, fp)

def calc_new_metric(g):

    r = 8

    rootdir_cents = "../../results/iscc-"+str(int(g/4))+"_hash-"+str(g)+"/centroids"

    print("Centroids calcolated with MEDIAN:")
    new_metric_cents_median(r,g,rootdir_cents)
    # print("Centroids calcolated with MEAN:")
    # new_metric_cents_mean(r,g,rootdir_cents)






