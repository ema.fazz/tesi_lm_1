from id_gen_hash import *
from hamming_mean_hash import hamming_mean_hash
from centroids_hash import *
from centroids_hash_median import *
from new_metric_hash import *
    

from plot_centroids_hash import *


for g in [8,16]:

    main_id_gen_hash(g)
    hamming_mean_hash(g)
    # # centroids_mean(g)
    centroids_median(g)
    calc_new_metric(g)
    plot_centroids(g)