import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pprint
import json
from scipy.spatial.distance import hamming

"""

CENTROID
    transform the r bit string in the corrispondent number, calc the mean between the ids number and transfrom the mean in a bit num

    [001] [110] [111]
      1     6     7   = 14/3 = 4 = [100]

"""

def create_centroids_json(i, g, r_dim_csvs, save_json):

    centroids = {}

    while i<=1024: #1024

        r_dir = "r-"+str(i)
        new_r_dir = os.path.join(r_dim_csvs, r_dir)
        centroids[r_dir] = {}

        g_dir = "g-"+str(g)
        g_path = os.path.join(new_r_dir, g_dir)
        centroids[r_dir][g_dir] = {}

        hash_dir = "hash-csvs"
        hash_path = os.path.join(g_path, hash_dir)
        
        for file in os.listdir(hash_path):

            if "hamming" not in file:

                csv_path=os.path.join(hash_path,file)
                print(csv_path)
                
                df = pd.read_csv(csv_path)

                final_r = df['Final-R']
                ids_to_num = []


                for el in final_r:
                    bit_n = ""
                    for j in el:
                        if j in ['0','1']:
                            bit_n += str(j)
                    int_n = int(bit_n,2)
                    ids_to_num.append(int_n)

                mean = int(np.mean(ids_to_num))

                stri = "" 
                for l in ids_to_num:
                    stri += str(l) + " + "
                stri += "=" + str(mean)
                print(stri)

                getbinary = lambda x, n: format(x, 'b').zfill(n)

                centroid_node = getbinary(mean, i)

                centroids[r_dir][g_dir][str(file)] = {
                    "centroid":str(centroid_node),
                    }

                print(centroid_node)
        
        i *= 2

    with open(os.path.join(save_json,'centroids-hash.json'), 'w') as fp: # change result-hash with result-iscc to save the iscc results 
        json.dump(centroids, fp)


def calc_hamming_centroids(i, g, save_json):

    centroids = json.load(open(os.path.join(save_json,'centroids-hash.json'), 'r'))

    while i<=1024: #1024

        r_dir = "r-"+str(i)

        g_dir = "g-"+str(g)

        keys = centroids[r_dir][g_dir].keys()

        for k in keys:
            
            k_centr = centroids[r_dir][g_dir][k]['centroid']
            centroids[r_dir][g_dir][k]['hamming_cents'] = {}
            #print(k_centr)
            for kj in keys:
                if kj != k:
                    kj_centr = centroids[r_dir][g_dir][kj]['centroid']
                    
                    print(list(k_centr), "   ", list(kj_centr))
                    ham_k_kj = hamming(list(k_centr), list(kj_centr)) * len(list(k_centr))
                    centroids[r_dir][g_dir][k]['hamming_cents'][kj] = ham_k_kj
            

        i *= 2
    
    with open(os.path.join(save_json,'centroids-hash.json'), 'w') as fp: # change result-hash with result-iscc to save the iscc results 
        json.dump(centroids, fp)


def centroids_mean(g):

    i = 8
    r_dim_csvs = "../../results/iscc-"+str(int(g/4))+"_hash-"+str(g)+"/csvs"

    save_json = "../../results/iscc-"+str(int(g/4))+"_hash-"+str(g)+"/centroids"

    

    create_centroids_json(i, g, r_dim_csvs, save_json)

    calc_hamming_centroids(i, g, save_json)