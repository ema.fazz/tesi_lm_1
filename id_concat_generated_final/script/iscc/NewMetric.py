import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pprint
import json
from scipy.spatial.distance import hamming


"""

results = []
for each class i
   
    distanza_da_altre_classi = []
    for each class j != i
        distanza_da_altre_classi.append( hamming(centroide(i),centroide(j)) )
   
    distanza_interna = []
    for each image k != centroide(i)
        distanza_interna.append( hamming(centroide(i), k) )

    results.append( avg(distanza_da_altre_classi) - avg(distanza_interna) )

metrica_finale = avg(results)

"""

class NewMetric():

    def new_metric_cents_median(rootdir_cents):

        centroid_json = os.path.join(rootdir_cents, "centroids-median-iscc.json")

        result_json = json.load(open(centroid_json, 'r'))

        dictj = result_json

        

        img_cls = dictj.keys()
        hamming_dict = {}

        new_metric = 0

        for k in list(img_cls):
            
            """  MEAN DISTANCE FOR CENTROIDS  """
            hamming_dict = dictj[k]['hamming_cents']
            hamming_mean_cents = np.mean(list(hamming_dict.values()))

            """  MEAN DISTANCE FROM CLASS CENTROID TO THE IMAGES  """
            k_csv = "../../results/iscc/csvs/"+k
            df = pd.read_csv(k_csv)

            final_r = df['Final-R']
            k_centr = dictj[k]['centroid']
            sum_hamming = 0
            for el in final_r:
                
                el = [str(i) for i in k_centr]
                #print(el, k_centr)
                sum_hamming += hamming(list(k_centr), list(el)) * len(list(k_centr))
            
            avg_internal_dist = sum_hamming / len(final_r)
            #print(k)
            new_metric += (hamming_mean_cents - avg_internal_dist)
        
        new_metric = new_metric/len(img_cls)
        new_metric_json = {}
        new_metric_json['value'] = new_metric

        with open(os.path.join(rootdir_cents,'new-metric-iscc.json'), 'w') as fp: # change result-hash with result-iscc to save the iscc results 
            json.dump(new_metric, fp)







