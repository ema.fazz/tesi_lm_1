
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pprint
import json


def hamming_mean_hash(g):

    # r_dim_csvs = "../../results/iscc-2_hash-8"
    r_dim_csvs = "../../results/hash/csvs"
    

    hamming_mean_dict = {}

    R = 256 # number of bits into the bit string

    for file in os.listdir(r_dim_csvs): 
        
        if "hamming" in str(file):

            csv = os.path.join(r_dim_csvs, file) # 

            hamming_mean_dict[str(file)] = {}
            df = pd.read_csv(csv) 
            j = 0

            for idx, row in df.iterrows():

                if idx % len(df.keys()) == 0:

                    tmp_df = pd.DataFrame(columns=df.keys())
                    arr = df[idx:(idx+len(df.keys()))][:]
                    tmp_df = pd.DataFrame(arr)
                    tmp_df = tmp_df.set_index('Unnamed: 0')
                    code = tmp_df.loc['Type'][0] # take the type of the code 
                    tmp_df = tmp_df.drop('Type',axis=0) # drop the type given that is write in the title
                    tmp_df = tmp_df.astype(np.float64) # cast all element to float 
                    
                    #tmp_df = tmp_df.mask(np.equal(*np.indices(tmp_df.shape))) # mask the diagonal
                    df_lower = np.tril(tmp_df)
                    df_lower[df_lower == 0] = np.nan
                    mean = np.nanmean(df_lower)
                    std = np.nanstd(df_lower)
                    hamming_mean_dict[str(file)][code+"-mean"] = mean
                    hamming_mean_dict[str(file)][code+"-std"] = std

        # pprint.pprint(hamming_mean_dict)

        result = "../../results/hash/hamming-mean-hash.json"
        with open(result, 'w') as fp: 
            json.dump(hamming_mean_dict, fp)
