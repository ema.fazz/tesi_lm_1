from asyncore import read
from binascii import hexlify
from operator import index
import hashlib as hl
from data_url import DataURL
import os
import base64
import random
import pandas as pd
from scipy.spatial.distance import hamming


def calc_node_id(file_path, r, g):  

    with open(file_path,"rb") as f:
        bytes = f.read() # read entire file as bytes
        readable_hash = hl.sha256(bytes).hexdigest()

    k = 0
    j = 1
    decomposed_hash = []
    while k < len(readable_hash):
        decomposed_hash.append(readable_hash[k:(g*j)])
        k = g*j
        j += 1

    r_bit = [0 for i in range(0,r)]

    for el in decomposed_hash:
        mod = int(el, 16) % r
        r_bit[mod] = 1

    return readable_hash, r_bit


# Create a csv file for every folder with simil images inside and save them into the "csvs" folder 
def create_csv(rootdir, r, csv_folder, g):

    for root, subFolders, files in os.walk(rootdir):
        i = 0
        for dir in subFolders:
            print(dir)
            
            dir_path = os.path.join(rootdir, dir)
            dir_df = pd.DataFrame(columns = ['Img-Name', 'SHA256', 'Final-R'])

            for file in os.listdir(dir_path):
                readable_hash, r_bit = calc_node_id(os.path.join(dir_path, file), r, g)

                file_codes = [str(file), str(readable_hash),str(r_bit)]

                dir_df.loc[i] = file_codes
                i += 1
    
            print("\n\n")
            print(dir_df)
            dir_df.to_csv(csv_folder+"/"+str(dir)+".csv", index=False)


# Calc hamming distance between codes and create distance matrices
def code_hamming_distance(csvs, hamming_csv):

    for file in os.listdir(csvs):

        if "hamming" not in str(file):

            hamming_file = str(file)[:-4] + "_hamming.csv"

            df_codes = pd.read_csv(os.path.join(csvs, file))
            idx_h_df = list(df_codes['Img-Name'])
            idx_h_df.insert(0,'Type')
            cols = idx_h_df[1:]
            df_codes = df_codes.set_index('Img-Name')

            concat_df = pd.DataFrame()

            for col in df_codes.keys():

                sep = [col for i in range(0,len(cols))]
                h_df = pd.DataFrame(columns=cols, index=idx_h_df)
                h_df.loc['Type'] = sep  

                for idx, row in df_codes.iterrows():
                    img = idx
                    str1 = str(df_codes.loc[img,col]).replace(' ','').replace('[','').replace(']','').replace(',','')

                    for idx, row in df_codes.iterrows():
                        
                        str2=str(df_codes.loc[idx,col]).replace(' ','').replace('[','').replace(']','').replace(',','')
                        # print(str1, "    ", str2)
                        # hamming(x, y) * len(x) -> * len(x) because hamming() returns the percentage of elements that differs
                        h_dist = hamming(list(str1),list(str2)) * len(list(str1))
                        # print(h_dist)
                        h_df.loc[img,idx] = round(h_dist)


                concat_df = pd.concat([concat_df,h_df], ignore_index=False)

            print(concat_df)
            concat_df.to_csv(os.path.join(hamming_csv, hamming_file))

def main_id_gen_hash(g):
    
    rootdir = "../../../photos"
    # csvs = "./iscc-distance-files/csvs"
    # hamming_csvs = "./iscc-distance-files/hamming_dist_csv/"
    # r_dim_csvs = "../../results/iscc-2_hash-8"
    r_dim_csvs = "../../results/hash/csvs"
    
    R = 256

    # # Create a csv file for every folder with simil images inside and save them into the "csvs" folder 
    create_csv(rootdir, R, r_dim_csvs, g)
    
    # # Calc hamming distance between codes and create distance matrices
    code_hamming_distance(r_dim_csvs, r_dim_csvs)


    

    



        








