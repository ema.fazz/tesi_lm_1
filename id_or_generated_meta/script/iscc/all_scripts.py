
import os
from IdGen import IdGen as ig
from HammingMean import HammingMean as hm
from Centroids import Centroids as cds
from NewMetric import NewMetric as nm
from PlotCentroids import PlotCentroids as pc
import json
import pprint


rootdir = "../../../photos"

for g in [2]:

    results = "../../results/iscc-"+str(g)+"_hash-"+str(g*4)

    results_csvs = os.path.join(results,"csvs")
    results_hamming_mean = os.path.join(results,"hamming_mean")
    results_centroids = os.path.join(results, "centroids")
    results_centroids_plots = os.path.join(results, "centroids_plots/iscc")

    all_hamming_mean = {}

    centroids_median = {}


    r = 8

    while r<=1024:
        
        r_dir = "r-"+str(r)
        new_r_dir = os.path.join(results_csvs, r_dir)
        if not os.path.exists(new_r_dir):
            os.mkdir(new_r_dir)

        g_dir = "g-"+str(g)
        g_path = os.path.join(new_r_dir, g_dir)
        if not os.path.exists(g_path):
            os.mkdir(g_path)

        iscc_dir = "iscc-csvs"
        iscc_path = os.path.join(g_path, iscc_dir)
        if not os.path.exists(iscc_path):
            os.mkdir(iscc_path)

        # # Create a csv file for every folder with simil images inside and save them into the "csvs" folder 
        ig.create_csv(ig, rootdir, r, iscc_path, g)
        
        # # Calc hamming distance between codes and create distance matrices
        ig.code_hamming_distance(ig, iscc_path, iscc_path)
        
        all_hamming_mean[r_dir] = hm.calc_hamming_mean(hm, r, g, results_csvs) 
        # centroids_mean[r_dir] = cds.centroids_mean(r, g, results_csvs)
        centroids_median[r_dir] = cds.centroids_median(r, g, results_csvs)

        r *= 2


    with open(os.path.join(results_hamming_mean,"hamming-mean-iscc.json"), 'w') as fp:
        json.dump(all_hamming_mean, fp)

    # # # with open(os.path.join(results_centroids,"centroids-mean-iscc.json"), 'w') as fp:
        # # # json.dump(centroids_mean, fp)

    with open(os.path.join(results_centroids,"centroids-median-iscc.json"), 'w') as fp:
        json.dump(centroids_median, fp)

    # cds.hamming_centroids_mean(g, results_centroids)
    cds.hamming_centroids_median(g, results_centroids)


    # another loop because i need the centroids json builted to calc the new metric and plots
    r = 8 
    # # new_metric_centroids_median = {}

    while r<=1024:

        r_dir = 'r-'+str(r)

        # # # new_metric_centroids_mean[r_dir] = nm.new_metric_cents_mean(r, g, results_centroids)
        # # new_metric_centroids_median[r_dir] = nm.new_metric_cents_median(r, g, results_centroids) # wrong method
        # # pprint.pprint(new_metric_centroids_median)
        # # # pc.plot_centroids_mean(r,g,results_centroids,results_centroids_plots)
        pc.plot_centroids_median(r,g,results_centroids,results_centroids_plots)

        r *= 2

    # # # with open(results+"/new_metric/new_metric_iscc_mean.json", 'w') as fp: 
    # # #     json.dump(new_metric_centroids_mean, fp)

    # # # with open(results+"/new_metric/new_metric_iscc_median.json", 'w') as fp: 
    # # #     json.dump(new_metric_centroids_median, fp)






    