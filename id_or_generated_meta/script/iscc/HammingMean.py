
from cmath import nan
from math import isnan
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pprint
import json


class HammingMean():

    def calc_hamming_mean(self, r, g, r_dim_csvs):

        r_dir = "r-"+str(r)
        new_r_dir = os.path.join(r_dim_csvs, r_dir)

        hamming_mean_dict = {}

        g_dir = "g-"+str(g)
        g_path = os.path.join(new_r_dir, g_dir)
        hamming_mean_dict[g_dir] = {}

        iscc_dir = "iscc-csvs"
        iscc_path = os.path.join(g_path, iscc_dir)

        for file in os.listdir(iscc_path): 
            
            if "hamming" in str(file):

                csv = os.path.join(iscc_path, file) 

                hamming_mean_dict[g_dir][str(file)] = {}
                df = pd.read_csv(csv)
                j = 0

                for idx, row in df.iterrows():

                    if idx % len(df.keys()) == 0:

                        tmp_df = pd.DataFrame(columns=df.keys())
                        arr = df[idx:(idx+len(df.keys()))][:]
                        tmp_df = pd.DataFrame(arr)
                        tmp_df = tmp_df.set_index('Unnamed: 0')
                        code = tmp_df.loc['Type'][0] # take the type of the code 
                        tmp_df = tmp_df.drop('Type',axis=0) # drop the type given that is write in the title
                        tmp_df = tmp_df.astype(np.float64) # cast all element to float 

                        #tmp_df = tmp_df.mask(np.equal(*np.indices(tmp_df.shape))) # mask the diagonal
                        df_lower = np.tril(tmp_df)
                        df_lower[df_lower == 0] = np.nan
                        mean = np.nanmean(df_lower)
                        std = np.nanstd(df_lower)
                        if isnan(mean):
                            hamming_mean_dict[g_dir][str(file)][code+"-mean"] = 0
                        else:
                            hamming_mean_dict[g_dir][str(file)][code+"-mean"] = mean
                        if isnan(std):
                            hamming_mean_dict[g_dir][str(file)][code+"-std"] = 0
                        else:
                            hamming_mean_dict[g_dir][str(file)][code+"-std"] = std
                        
            # pprint.pprint(hamming_mean_dict)
        return hamming_mean_dict

                



    

    