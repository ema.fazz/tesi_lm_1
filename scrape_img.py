import requests
from bs4 import BeautifulSoup
from PIL import Image
from io import BytesIO
import urllib
import os


def imagedown(URL, folder):
    print(URL)
    USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:65.0) Gecko/20100101 Firefox/65.0"
    headers = {"user-agent": USER_AGENT}
    resp = requests.get(URL, headers=headers)
    soup = BeautifulSoup(resp.content, "html.parser")
    #print(soup)
    wow = soup.find_all('a',class_='iusc')
    k = 0 
    N_IMGS = 15 # num of images to download for every folder
    j = 0 # index to scroll the images links
    while k<N_IMGS:
        i = wow[j]
        try:
            #print(eval(i['m'])['murl'])
            img_obj = requests.get(eval(i['m'])['murl'])
            img = Image.open(BytesIO(img_obj.content))
            path = os.path.join("./photos", folder)
            print(img.format)
            if not os.path.exists(path):
                os.mkdir(path)
            if str(img.format) == "JPEG":
                print("IMG: "+ img.format)
                img.save(path+"/"+str(folder)+str(k)+".jpg", img.format)
                k += 1
        except:
            pass
        j+=1

if __name__ == "__main__":

    URLS={

        "san_petronio":"https://www.bing.com/images/search?q=san%20petronio%20bologna&qs=n&form=QBIR&sp=-1&pq=san%20petronio%20bol&sc=10-16&cvid=CB7468F064E74DE1AFCEAC4A0F1A2B7A&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "san_luca":"https://www.bing.com/images/search?q=san+luca+bologna&qs=LS&form=QBILPG&sp=1&ghc=1&pq=san+luca+&sc=10-9&cvid=8D1B8E033A7340CFB7681D6ED903D39C&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "due_torri":"https://www.bing.com/images/search?q=due%20torri%20bologna&qs=n&form=QBIR&sp=-1&pq=due%20torri%20bologna&sc=7-17&cvid=50CDC1BA17FC4294A9F0E32F3519206D&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "torre_pisa":"https://www.bing.com/images/search?q=torre+di+pisa&qs=n&form=QBILPG&sp=-1&pq=torre+di+pisa&sc=10-13&cvid=98DCAB91F3F34C508E144BA9E5B2E6EC&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "colosseo":"https://www.bing.com/images/search?q=colosseo&qs=n&form=QBIR&sp=-1&pq=colosseo&sc=10-8&cvid=8154879581034310B55D34829E63EC86&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "duomo_milano":"https://www.bing.com/images/search?q=duomo%20di%20milano&qs=n&form=QBIR&sp=-1&pq=duomo%20di%20milano&sc=10-15&cvid=1A0650E6E7884FD1BDDE8B6643257125&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",
        
        "monalisa":"https://www.bing.com/images/search?q=monalisa&qs=n&form=QBIR&sp=-1&pq=monalisa&sc=10-8&cvid=CD114F1F5E3446E385891CBD69E0E5A1&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "urlo_munch":"https://www.bing.com/images/search?q=urlo%20di%20munch&qs=AS&form=QBIR&sp=1&ghc=1&pq=urlo%20di%20mu&sc=10-10&cvid=3533DA2586FF44A18B1B90C82D15B784&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "notte_stellata_vangog":"https://www.bing.com/images/search?q=notte%20stellata%20vangog&qs=n&form=QBIR&sp=-1&sc=0-0&cvid=F5A9F83015794F02A4E24171A06A704C&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "piazza_san_marco":"https://www.bing.com/images/search?q=piazza%20san%20marco&qs=n&form=QBIR&sp=-1&ghc=1&pq=piazza%20san%20marco&sc=10-16&cvid=173897B19B4948ABB0EA5852A897490E&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "basilica_san_marco":"https://www.bing.com/images/search?q=basilica%20san%20marco&qs=n&form=QBIR&sp=-1&pq=basilica%20san%20marco&sc=10-18&cvid=20253C4041E34668A452351DFE985A0E&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "ultima_cena":"https://www.bing.com/images/search?q=ultima%20cena%20leonardo&qs=LS&form=QBIR&sp=2&ghc=1&pq=ultima%20cena&sc=10-11&sk=AS1&cvid=B0814D402232494590A6D47B649DA612&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "cinque_terre":"https://www.bing.com/images/search?q=cinque%20terre&qs=n&form=QBIR&sp=-1&ghc=1&pq=cinque%20ter&sc=10-10&cvid=C81D8CDC805F4B379E1EF671BB0E4D59&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "fontana_trevi":"https://www.bing.com/images/search?q=fontana%20di%20trevi&qs=n&form=QBIR&sp=-1&pq=fontana%20di%20trevi&sc=10-16&cvid=4E201E71AA7B47C98C0D529DF87B55C5&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "nettuno":"https://www.bing.com/images/search?q=nettuno%20bologna&qs=n&form=QBIR&sp=-1&pq=nettuno%20bologna&sc=10-15&cvid=9994D43E360C4BD495E98B8270F5FC4D&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "basilica_santa_croce":"https://www.bing.com/images/search?q=basilica%20di%20santa%20croce%20firenze&qs=LS&form=QBIR&sp=1&pq=basilica%20di%20santa%20croce&sc=10-23&cvid=0334BDF8BE1442B4A9C8C2063922E312&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "altare_della_patria":"https://www.bing.com/images/search?q=altare%20della%20patria&qs=n&form=QBIR&sp=-1&pq=altare%20della%20patria&sc=10-19&cvid=2738E5AA45674E45A80CA6B6444B0EC4&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "arena_verona":"https://www.bing.com/images/search?q=arena%20di%20verona&qs=n&form=QBIR&sp=-1&ghc=1&pq=arena%20di%20verona&sc=10-15&cvid=39C316D911814022B1C1085DEE65E994&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",
        
        "lavorare_lavorare_lavorare":"https://www.bing.com/images/search?q=lavorare%20lavorare%20lavorare%20preferisco%20il%20rumore%20del%20mare&qs=n&form=QBIR&sp=-1&pq=lavorare%20lavorare%20lavorare%20preferisco%20il%20rumore%20del%20mare&sc=0-56&cvid=251A9F4953414C5B88238DFEE23D0D16&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "reggia_caserta":"https://www.bing.com/images/search?q=reggia%20di%20caserta&qs=n&form=QBIR&sp=-1&pq=reggia%20di%20caserta&sc=10-17&cvid=842D4B9182484B4B98263577F5452C0A&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "arco_pace":"https://www.bing.com/images/search?q=arco%20della%20pace%20milano&qs=AS&form=QBIR&sp=2&pq=arco%20della%20pace&sc=10-15&sk=AS1&cvid=5896A3C129FB45C3A8CDD595DD879BF4&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "hamburger":"https://www.bing.com/images/search?q=hamburger&qs=n&form=QBIR&sp=-1&pq=hamburger&sc=10-9&cvid=7F776437DD4F494C85DF23E7F56AA53D&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "pantheon":"https://www.bing.com/images/search?q=pantheon&qs=AS&form=QBIR&sp=1&ghc=1&pq=panthe&sc=10-6&cvid=E91C57960DFA49F18DDD5FD5E1CA3AD7&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "ponte_vecchio":"https://www.bing.com/images/search?q=ponte%20vecchio&qs=n&form=QBIR&sp=-1&ghc=1&pq=ponte%20vecchio&sc=10-13&cvid=A4557EC666204980B8ECDDA90D37F2FF&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "il_bacio":"https://www.bing.com/images/search?q=il%20bacio%20klimt&qs=AS&form=QBIR&sp=1&pq=il%20bacio%20klim&sc=10-13&cvid=5BA72FF6B3124D90B89B55AB00281C69&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "taj_mahal":"https://www.bing.com/images/search?q=taj%20mahal&qs=AS&form=QBIR&sp=1&pq=taj&sc=10-3&cvid=1EFA25236B7B402C97317330EBFC1498&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "pietà_mic":"https://www.bing.com/images/search?q=piet%C3%A0+michelangelo&qs=n&form=QBILPG&sp=-1&pq=piet%C3%A0+michelangelo&sc=10-18&cvid=C3F02CBBFFED4DE39999895A5647D20C&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "statua_libertà":"https://www.bing.com/images/search?q=statua%20della%20libert%C3%A0&qs=AS&form=QBIR&sp=1&ghc=1&pq=statua%20della%20li&sc=10-15&cvid=F367AF99AECE413E989503B1A0A7A878&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",

        "arco_trionfo":"https://www.bing.com/images/search?q=arco%20di%20trionfo&qs=LS&form=QBIR&sp=1&pq=arco%20di%20trionfo&sc=10-15&cvid=3725E56D795347C18493AB89A56B74E9&first=1&tsc=ImageHoverTitle",

        "obelisco_wash_dc":"https://www.bing.com/images/search?q=monumento%20a%20washington&qs=n&form=QBIR&sp=-1&pq=monumento%20a%20washington&sc=10-22&cvid=38B7CFB7C7DD414E90FDA6DAECEB654A&ghsh=0&ghacc=0&first=1&tsc=ImageHoverTitle",
    }

    # folders = URLS.keys()

    # for dir in folders:
    #     url = URLS[dir]
    #     imagedown(url, dir)
    
    dirs = []
    for dir in os.listdir('photos'):
        dirs.append(dir)

    searches = {}
    for k in URLS.keys():
        search = URLS[k]
        query = search[37:search.index('&')]
        searches[k] = query.replace('%20', ' ').replace('+',' ').replace('%C3%A0','à')

    for dir in dirs:
        query = searches[dir]
        print(dir.replace('_','\\_') + ' & '+ query + ' \\\\')

    
