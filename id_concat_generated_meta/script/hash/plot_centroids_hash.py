from cProfile import label
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sn
from pathlib import Path
import json


""" 
def plot_centroids_mean(g, rootdir, savedir):

    centroid_json = os.path.join(rootdir, "centroids-hash.json")

    result_json = json.load(open(centroid_json, 'r'))

    r=256

    dictj = result_json

    img_cls = dictj.keys()
    print(img_cls)
    hamming_dict = {}

    for k in list(img_cls)[:5]:

        figure, axis = plt.subplots(2, figsize=(40,20))
        hamming_dict = {}
        hamming_dict = dictj[k]['hamming_cents']

        IMAGE CLASSES DISTANCES
        print(hamming_dict)
        ax = 0
        xAxis = [key for key, value in hamming_dict.items()]
        yAxis = [value for key, value in hamming_dict.items()]
        axis[ax].plot(xAxis,yAxis, color='maroon', marker='o')
        axis[ax].set_title("Hamming between classes - from "+str(k))
        axis[ax].set_xticklabels(xAxis,rotation=50)
        
        DISTANCES BETWEEN IMAGES IN THE SAME CLASS

        go_up_dir = Path(rootdir).parents[0]
        k_csv = str(go_up_dir) + "/csvs/"+k[:-4]+"_hamming.csv"
        #print(k_csv)
        df = pd.read_csv(k_csv)
        ax = 1

        for idx, row in df.iterrows():

                if idx % len(df.keys()) == 0:

                    tmp_df = pd.DataFrame(columns=df.keys())
                    arr = df[idx:(idx+len(df.keys()))][:]
                    tmp_df = pd.DataFrame(arr)
                    tmp_df = tmp_df.set_index('Unnamed: 0')
                    code = tmp_df.loc['Type'][0] # take the type of the code 
                    if code == 'Final-R':
                        tmp_df = tmp_df.drop('Type',axis=0) # drop the type given that is write in the title
                        tmp_df = tmp_df.astype(np.float64) # cast all element to float
                        #print(tmp_df)
                        cm = sn.heatmap(tmp_df, annot=True)
                        cm.set_xticklabels(labels=cm.get_xticklabels(), rotation=50)
                        figure.tight_layout()
                        figure.savefig(savedir+"/"+str(k)[:-4]) 
"""


def plot_centroids_median(g, rootdir, savedir):

    centroid_json = os.path.join(rootdir, "centroids-hash-median.json")

    result_json = json.load(open(centroid_json, 'r'))

    r=256

    dictj = result_json

    img_cls = dictj.keys()
    #print(img_cls)
    hamming_dict = {}

    for k in list(img_cls)[:5]:
        figure, axis = plt.subplots(2, figsize=(40,20))
        hamming_dict = {}
        hamming_dict = dictj[k]['hamming_cents']

        """ IMAGE CLASSES DISTANCES """
        print(hamming_dict)
        ax = 0
        xAxis = [key for key, value in hamming_dict.items()]
        yAxis = [value for key, value in hamming_dict.items()]
        axis[ax].plot(xAxis,yAxis, color='maroon', marker='o')
        axis[ax].set_title("Hamming between classes - from "+str(k))
        axis[ax].set_xticklabels(xAxis,rotation=50)
        
        """ DISTANCES BETWEEN IMAGES IN THE SAME CLASS """

        go_up_dir = Path(rootdir).parents[0]
        k_csv = str(go_up_dir) + "/csvs/"+k[:-4]+"_hamming.csv"
        #print(k_csv)
        df = pd.read_csv(k_csv)
        ax = 1

        for idx, row in df.iterrows():

                if idx % len(df.keys()) == 0:

                    tmp_df = pd.DataFrame(columns=df.keys())
                    arr = df[idx:(idx+len(df.keys()))][:]
                    tmp_df = pd.DataFrame(arr)
                    tmp_df = tmp_df.set_index('Unnamed: 0')
                    code = tmp_df.loc['Type'][0] # take the type of the code 
                    if code == 'Final-R':
                        tmp_df = tmp_df.drop('Type',axis=0) # drop the type given that is write in the title
                        tmp_df = tmp_df.astype(np.float64) # cast all element to float
                        #print(tmp_df)
                        cm = sn.heatmap(tmp_df, annot=True)
                        cm.set_xticklabels(labels=cm.get_xticklabels(), rotation=50)
                        figure.tight_layout()
                        figure.savefig(savedir+"/"+str(k)[:-4])

def plot_centroids(g):

    rootdir_cents = "../../results/hash"

    savedir = rootdir_cents+"_plots/hash"
    
    if not os.path.exists(savedir):
        os.mkdir(savedir)
    
    plot_centroids_median(g, rootdir_cents, savedir)
    # plot_centroids_mean(g, rootdir_cents, savedir)
    


