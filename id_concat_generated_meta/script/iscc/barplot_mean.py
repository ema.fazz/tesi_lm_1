
import matplotlib.pyplot as plt
import json

def barplot_concat_id():

    file1 = "../../../id_or_generated_meta/results/iscc-2_hash-8/hamming_mean/hamming-mean-iscc.json"
    file2 = "../../../id_or_generated_meta/results/iscc-4_hash-16/hamming_mean/hamming-mean-iscc.json"
    file3 = "../../results/iscc/hamming-mean-iscc.json"

    json_file1 = json.load(open(file1, 'r'))
    json_file2 = json.load(open(file2, 'r'))
    json_file3 = json.load(open(file3, 'r'))

    json_file1 = json_file1['r-256']['g-2']
    json_file2 = json_file2['r-256']['g-4']

    keys = json_file1.keys()

    for k in keys:
        
        imgs1 = json_file1[k]
        imgs2 = json_file2[k]
        imgs3 = json_file3[k]
        fig, axs = plt.subplots(3, figsize=(20,10))

        axs[0].set_title('iscc-g2')
        axs[1].set_title('iscc-g4')
        axs[2].set_title('iscc-concat')

        axs[0].bar(imgs1.keys(),imgs1.values(), color='b', width=0.4)
        axs[1].bar(imgs2.keys(),imgs2.values(), color='g', width=0.4)
        axs[2].bar(imgs3.keys(),imgs3.values(), color='r', width=0.4)

        fig.tight_layout()

        fig.savefig('../../results/iscc/barplots/'+k[:-4])