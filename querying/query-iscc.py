from binascii import hexlify
import json
from pprint import pprint
import random
import iscc_sdk as idk
from data_url import DataURL
import jcs
import base64
import pandas as pd
import os
from openlocationcode import openlocationcode as olc
from scipy.spatial.distance import hamming
import ipfshttpclient
import networkx as nx
import matplotlib.pyplot as plt


def calcISCC(path, title, description, metas):

        meta_serialized = jcs.canonicalize(metas)
        
        du_obj = DataURL.from_data('application/json',base64_encode=True, data=meta_serialized)

        ic_meta = idk.IsccMeta(name=title,description=description,meta=du_obj.url) 
        iscc_meta = idk.embed_metadata(path,ic_meta)

        iscc_meta = idk.code_meta(iscc_meta).dict()
        
        base64_metas = iscc_meta['meta'][iscc_meta['meta'].index(',')+1:]

        decoded_metas = base64.b64decode(base64_metas).decode('ascii')

        print("base64 decoded metadata: "+decoded_metas+"\n\n")

        return iscc_meta['iscc'][5:]

def calc_node_id(iscc_meta, r, g):

    # check that g is between [1,15]
    if g > 15:
        g = 15
    elif g < 1:
        g = 1

    iscc_decomposed_meta = []
    tmp_str_meta = ""

    for i in range(0,len(iscc_meta)): # meta and content have the same length

        # create the decomposed iscc string to calculate the hash and then the node ID 
        tmp_str_meta += iscc_meta[i]  

        if (i+1) % g == 0:
            iscc_decomposed_meta.append(tmp_str_meta)
            tmp_str_meta = ""
        
        # if r-bit string is odd then it have to append the last char to the last decomposed iscc string
        if g % 2 != 0 and i == len(iscc_meta)-1:
            iscc_decomposed_meta[len(iscc_decomposed_meta)-1] += iscc_meta[i]

    #print(iscc_decomposed)

    # Calc the node ID where the file will be saved
    r_bit_posisitons_meta = []

    for i in range(0, len(iscc_decomposed_meta)): # meta and content have the same length
        
        deco_hex_meta = hexlify(iscc_decomposed_meta[i].encode()).decode()
        val_int_meta = int(deco_hex_meta,16) # base 16 for hexa string
        mod_meta = val_int_meta % r
        r_bit_posisitons_meta.append(mod_meta)

    r_string_meta = [0 for i in range(0,r)]

    for el in r_bit_posisitons_meta:
        r_string_meta[el] = 1
    

    return r_string_meta


# Create a csv file for every folder with simil images inside and save them into the "csvs" folder 
def create_csv(rootdir, r, g):
    
    i = 0
    
    n_dir = 0
    for dir in os.listdir(rootdir):
        dir_df = pd.DataFrame(columns = ['Img-Name','Meta','R-Meta','Location'])

        latlong = {
                "lat": round(random.uniform(-190,190), 6),
                "long": round(random.uniform(-190,190), 6),
            }    

        _olc = olc.encode(latlong['lat'], latlong['long'], 6)

        meta = {
                "@type":"Places",
                "olc": _olc
            }

        if n_dir <= 5 or dir == "san_petronio":
            for file in os.listdir(os.path.join(rootdir,dir)):

                file_path = rootdir+"/"+dir+"/"+file
                file_title = "Img " + str(file)

                iscc_meta = calcISCC(file_path,file_title,file_title,meta)
                r_string_m= calc_node_id(iscc_meta, r, g)

                file_codes = [str(file),iscc_meta,str(r_string_m),str(_olc)]

                dir_df.loc[i] = file_codes
                i += 1

            dir_df.to_csv("r-"+str(r)+"/iscc/csvs/"+dir+".csv", index=False)
        n_dir += 1

def calc_query_image_node(query_image, r):

    title_img = query_image[:-4]

    description = "san_petronio"

    class_images = pd.read_csv('r-'+str(r)+'/iscc/csvs/san_petronio.csv')

    _olc = class_images.loc[0]['Location']

    metas = {
            "@type":"Places",
            "olc": _olc
        }

    iscc_meta_test_img = calcISCC(query_image, title_img, description, metas)

    calc_node = calc_node_id(iscc_meta_test_img,r,g)

    return calc_node



def create_binary_id(n, r):
    binary_id = bin(n)[2:]
    while len(binary_id) < r:
        binary_id = '0' + binary_id
    return binary_id

def create_json_hypercube(nodes, r):

    json_hypercube = {}

    for n in nodes:
        node = str(n).replace("(","").replace(")","").replace(",","").replace(" ","")
        json_hypercube[node] = []

    with open('r-'+str(r)+'/iscc/hypercube.json', 'w') as f:
        json.dump(json_hypercube,f)

def add_obj_ipfs(path, ipfs_client):

    with open('r-'+str(r)+'/iscc/hypercube.json', 'r') as f:
            hypercube = json.load(f)

    sp_hash = {}
    # IMAGES ADDED TO IPFS
    for file in os.listdir('r-'+str(r)+'/iscc/csvs'):

        df = pd.read_csv('r-'+str(r)+'/iscc/csvs/'+file)
        i=0 
        
        for image in df['Img-Name']:
            print(image)
            obj_hash = ipfs_client.add(path+"/"+file[:-4]+"/"+image)['Hash']
            print(obj_hash)
            if "san_petronio" in file:
                sp_hash[image] = obj_hash
            image_node = str(df.iloc[i,2]).replace("[","").replace("]","").replace(",","").replace(" ","")
            hypercube[image_node].append(obj_hash)
            i+=1

    with open('r-'+str(r)+'/iscc/hypercube.json', 'w') as f:
        json.dump(hypercube,f)

    with open('r-'+str(r)+'/iscc/san_petronio_hashs.json', 'w') as f:
        json.dump(sp_hash,f)



    
def get_ipfs_image(hash, ipfs_client):
    # GET AN IMAGE BY ITS IPFS CID
    ipfs_client.get(hash, target='./download')




def search(graph, initial_id, keyword, hypercube, nodes, threshold=-1):

    hops = 0

    if threshold == -1:

        if keyword == initial_id:
            return 0, hypercube[initial_id]
        else:
            path_to_node = nx.shortest_path(graph, initial_id, keyword)
            hops = len(path_to_node)-1
            return hops, hypercube[path_to_node[-1:][0]]

    elif threshold > 0:

        results_object = []
        path_to_node = nx.shortest_path(graph, initial_id, keyword)
        hops += len(path_to_node)-1
        root = path_to_node[-1:][0]
        results_object.extend(hypercube[root])
        connected_nodes = graph.neighbors(root)

        visited = []
        while threshold > 0 and len(visited) < nodes:
            
            connected_nodes = list(connected_nodes)
            for node in connected_nodes:
            
                if node not in visited:
                    visited.append(node)
                    print(visited)
                    results_object.extend(hypercube[node])
                    hops += 1
                    threshold-=len(hypercube[node])
                    if threshold <= 0 or len(visited) >= nodes:
                        break   
                                 
            if threshold >= 0 or len(visited) <= nodes:
                children = []
                for node in connected_nodes:
                    children.extend(graph.neighbors(node))
                connected_nodes = children
        
        return hops, results_object



def first_time(rootdir,r,g,LABELS,ipfs_client):
    
    create_csv(rootdir, r, g)

    create_json_hypercube(LABELS, r)

    add_obj_ipfs(rootdir, ipfs_client)

def exec_query(graph, NODES,hypercube,true_obj, results, threshold):

    initial_id = ''
    for i in range(0,r):
        initial_id += '0'

    query_image_node = str(calc_query_image_node(query_image,r)).replace("[","").replace("]","").replace(",","").replace(" ","")
    hops_count_search, objects = search(graph, initial_id, query_image_node, hypercube, NODES, threshold)
    
    positive = 0
    for el in objects:
        if el in true_obj:
            positive += 1
            print(positive)

    results['r-'+str(r)] = {}
    results['r-'+str(r)]['query_image_node'] = query_image_node
    results['r-'+str(r)]['positive_objs'] = positive
    results['r-'+str(r)]['all_objs'] = len(objects)
    results['r-'+str(r)]['total_hops'] = hops_count_search


if __name__ == "__main__":

    query_image = "san_petronio.jpg"
    rootdir = '../photos/'
    r_list = [8] #[8, 12, 16]
    g = 4
    
    # addr = '/ip4/0.0.0.0/tcp/5001'
    # ipfs_client = ipfshttpclient.connect(addr)

    results = {}
    
    for r in r_list:

        HOPS = 0

        NODES = 2 ** r

        LABELS = {tuple(int(j) for j in create_binary_id(i, r))
                : create_binary_id(i, r) for i in range(0, NODES)}

        graph = nx.relabel_nodes(
                nx.generators.lattice.hypercube_graph(r), LABELS)

        # run the first time to create the csv file
        # first_time(rootdir,r,g,LABELS,ipfs_client)
        # end only first time

        hypercube = json.load(open('r-'+str(r)+'/iscc/hypercube.json', 'r'))
        true_obj = list(json.load(open('r-'+str(r)+'/iscc/san_petronio_hashs.json','r')).values())
        # threshold = -1 # >0 for Superset-search
        # exec the query on the DHT
        # exec_query(graph, NODES, hypercube, true_obj, results, threshold)
        
    # with open('results_query_json/results-iscc-pin-g'+str(g)+'.json', 'w') as f:
    #     json.dump(results,f)

    # ipfs_client.close()

