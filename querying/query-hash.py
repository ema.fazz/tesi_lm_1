
import json
import hashlib as hl
import pandas as pd
import os
import ipfshttpclient
import networkx as nx
import matplotlib.pyplot as plt


def calc_node_id(file_path, r, g):  

    with open(file_path,"rb") as f:
        bytes = f.read() # read entire file as bytes
        readable_hash = hl.sha256(bytes).hexdigest()

    k = 0
    j = 1
    decomposed_hash = []
    while k < len(readable_hash):
        decomposed_hash.append(readable_hash[k:(g*j)])
        k = g*j
        j += 1

    r_bit = [0 for i in range(0,r)]

    for el in decomposed_hash:
        mod = int(el, 16) % r
        r_bit[mod] = 1

    return readable_hash, r_bit


# Create a csv file for every folder with simil images inside and save them into the "csvs" folder 
def create_csv(rootdir, r, g):

    for root, subFolders, files in os.walk(rootdir):
        i = 0
        n_dir = 0
        for dir in subFolders:
            
            if n_dir <= 5 or dir == "san_petronio":

                dir_path = os.path.join(rootdir, dir)
                dir_df = pd.DataFrame(columns = ['Img-Name', 'SHA256', 'Final-R'])

                for file in os.listdir(dir_path):
                    readable_hash, r_bit = calc_node_id(os.path.join(dir_path, file), r, g)

                    file_codes = [str(file), str(readable_hash),str(r_bit)]

                    dir_df.loc[i] = file_codes
                    i += 1
        
                print("\n\n")
                print(dir_df)
                dir_df.to_csv("r-"+str(r)+"/hash/csvs/"+str(dir)+".csv", index=False)
            n_dir += 1

def calc_query_image_node(query_image, r, g):

    with open(query_image,"rb") as f:
        bytes = f.read() # read entire file as bytes
        readable_hash = hl.sha256(bytes).hexdigest()

    k = 0
    j = 1
    decomposed_hash = []
    while k < len(readable_hash):
        decomposed_hash.append(readable_hash[k:(g*j)])
        k = g*j
        j += 1

    r_bit = [0 for i in range(0,r)]

    for el in decomposed_hash:
        mod = int(el, 16) % r
        r_bit[mod] = 1

    return r_bit



def create_binary_id(n, r):
    binary_id = bin(n)[2:]
    while len(binary_id) < r:
        binary_id = '0' + binary_id
    return binary_id

def create_json_hypercube(nodes, r):

    json_hypercube = {}

    for n in nodes:
        node = str(n).replace("(","").replace(")","").replace(",","").replace(" ","")
        json_hypercube[node] = []

    with open('r-'+str(r)+'/hash/hypercube.json', 'w') as f:
        json.dump(json_hypercube,f)

def add_obj_ipfs(path, ipfs_client):

    with open('r-'+str(r)+'/hash/hypercube.json', 'r') as f:
            hypercube = json.load(f)

    sp_hash = {}
    # IMAGES ADDED TO IPFS
    for file in os.listdir('r-'+str(r)+'/hash/csvs'):

        df = pd.read_csv('r-'+str(r)+'/hash/csvs/'+file)
        i=0 
        
        for image in df['Img-Name']:
            print(image)
            obj_hash = ipfs_client.add(path+"/"+file[:-4]+"/"+image)['Hash']
            print(obj_hash)
            if "san_petronio" in file:
                sp_hash[image] = obj_hash
            image_node = str(df.iloc[i,2]).replace("[","").replace("]","").replace(",","").replace(" ","")
            hypercube[image_node].append(obj_hash)
            i+=1

    with open('r-'+str(r)+'/hash/hypercube.json', 'w') as f:
        json.dump(hypercube,f)

    with open('r-'+str(r)+'/hash/san_petronio_hashs.json', 'w') as f:
        json.dump(sp_hash,f)



    
def get_ipfs_image(hash, ipfs_client):
    # GET AN IMAGE BY ITS IPFS CID
    ipfs_client.get(hash, target='./download')




def search(graph, initial_id, keyword, hypercube, nodes, threshold=-1):

    hops = 0

    if threshold == -1:

        if keyword == initial_id:
            return 0, hypercube[initial_id]
        else:
            path_to_node = nx.shortest_path(graph, initial_id, keyword)
            hops = len(path_to_node)-1
            return hops, hypercube[path_to_node[-1:][0]]

    elif threshold > 0:
        results_object = []
        path_to_node = nx.shortest_path(graph, initial_id, keyword)
        hops += len(path_to_node)-1
        root = path_to_node[-1:][0]
        results_object.extend(hypercube[root])
        connected_nodes = graph.neighbors(root)

        visited = []
        while threshold > 0 and len(visited) < nodes:
            
            connected_nodes = list(connected_nodes)
            for node in connected_nodes:
            
                if node not in visited:
                    visited.append(node)
                    print(len(visited))
                    results_object.extend(hypercube[node])
                    hops += 1
                    threshold-=len(hypercube[node])
                    if threshold <= 0 or len(visited) >= nodes:
                        break                

            if threshold >= 0 or len(visited) <= nodes:
                children = []
                for node in connected_nodes:
                    children.extend(graph.neighbors(node))
                connected_nodes = children
        
        return hops, results_object

def first_time(rootdir,r,g,LABELS,ipfs_client):

    create_csv(rootdir, r, g)
    create_json_hypercube(LABELS, r)
    add_obj_ipfs(rootdir, ipfs_client)

def exec_query(graph, NODES,hypercube, true_obj, results, threshold):

    initial_id = ''
    for i in range(0,r):
        initial_id += '0'

    query_image_node = str(calc_query_image_node(query_image,r, g)).replace("[","").replace("]","").replace(",","").replace(" ","")
    hops_count_search, objects = search(graph, initial_id, query_image_node, hypercube, NODES, threshold)

    positive = 0
    for el in objects:
        if el in true_obj:
            positive += 1
            print(positive)
    
    results['r-'+str(r)] = {}
    results['r-'+str(r)]['query_image_node'] = query_image_node
    results['r-'+str(r)]['positive_objs'] = positive
    results['r-'+str(r)]['all_objs'] = len(objects)
    results['r-'+str(r)]['total_hops'] = hops_count_search




if __name__ == "__main__":

    query_image = "san_petronio.jpg"
    rootdir = '../photos/'
    r_list = [8] #[8, 12, 16]
    g = 16
    
    addr = '/ip4/0.0.0.0/tcp/5001'
    ipfs_client = ipfshttpclient.connect(addr)

    
    results = {}
    
    for r in r_list:

        HOPS = 0

        NODES = 2 ** r

        LABELS = {tuple(int(j) for j in create_binary_id(i, r))
                : create_binary_id(i, r) for i in range(0, NODES)}

        graph = nx.relabel_nodes(
                nx.generators.lattice.hypercube_graph(r), LABELS)

        # # # run the first time to create the csv file
        # first_time(rootdir,r,g,LABELS,ipfs_client)
        # # # end only first time

        # hypercube = json.load(open('r-'+str(r)+'/hash/hypercube.json', 'r'))
        # true_obj = list(json.load(open('r-'+str(r)+'/hash/san_petronio_hashs.json','r')).values())
        # threshold = -1 # > 0 for Superset-Searc
        # exec_query(graph, NODES, hypercube, true_obj, results, threshold)

    with open('results_query_json/results-hash-g'+str(g)+'.json', 'w') as f:
        json.dump(results,f)

    ipfs_client.close()

