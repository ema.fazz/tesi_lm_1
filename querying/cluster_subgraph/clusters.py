from binascii import hexlify
import json
from pprint import pprint
import random
import iscc_sdk as idk
from data_url import DataURL
import jcs
import base64
import pandas as pd
import os
from openlocationcode import openlocationcode as olc
from scipy.spatial.distance import hamming
import ipfshttpclient
import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib
import community as community_louvain

def create_json_cids(rootdir, check_iscc_or_hash, r):
    addr = '/ip4/0.0.0.0/tcp/5001'
    ipfs_client = ipfshttpclient.connect(addr)

    clss_cids = {}
    colors = ['red', 'yellow', 'skyblue', 'violet', 'aqua', 'lime', 'darkorange']

    i=0
    for file in os.listdir('../r-'+str(r)+'/'+check_iscc_or_hash+'/csvs'):

        df = pd.read_csv('../r-'+str(r)+'/'+check_iscc_or_hash+'/csvs/'+file)
        clss_cids[file[:-4]] = {}
        clss_cids[file[:-4]]['color'] = colors[i]
        i+=1
        
        for image in df['Img-Name']:
            cid = ipfs_client.add(rootdir+"/"+file[:-4]+"/"+image)['Hash']
            clss_cids[file[:-4]][image] = cid

    pprint(clss_cids)

    with open('cls_imgs_cids_'+check_iscc_or_hash+'-r'+str(r)+'.json', 'w') as f:
        json.dump(clss_cids,f)

def create_binary_id(n, r):
    binary_id = bin(n)[2:]
    while len(binary_id) < r:
        binary_id = '0' + binary_id
    return binary_id

def plot_graph(G, hypercube, cids, check_iscc_or_hash, r):

    clss_lists = {}
    black_list = 'color'
    nodes_color = {}
    for k in cids.keys():
        tmp_list = []
        for c, v in cids[k].items():
            if c != black_list:
                tmp_list.append(v)
                
        clss_lists[k] = tmp_list

    imgs_contained = {}
    #pprint(clss_lists)
    for k in hypercube.keys():
        if hypercube[k] != []:
            nodes_color[k] = ''
            imgs_contained[k] = len(hypercube[k])
            for c in clss_lists.keys():
                c_cids = clss_lists[c]
                if set(hypercube[k]).issubset(set(c_cids)):
                    nodes_color[k] = cids[c]['color']
                
    for k in nodes_color.keys():
        if nodes_color[k] == '':
            nodes_color[k] = 'darkgrey'
    pprint(nodes_color)

    #subgraph = G.subgraph(list(nodes_color.keys()))
    nodes = [k for k,v in nodes_color.items()]
    subgraph = G.subgraph(nodes)

    colors = ['red', 'yellow', 'skyblue', 'violet', 'aqua', 'lime', 'darkorange', 'darkgrey']
    for k,v in nodes_color.items():
        nodes_color[k] = colors.index(v)

    print(nodes_color)

    pos = nx.spring_layout(subgraph)
    cmap = matplotlib.colors.ListedColormap(colors)
    plt.figure(figsize=(18,12))
    nx.draw_networkx_nodes(subgraph,pos,nodes_color.keys(),cmap=cmap,node_color=list(nodes_color.values()))
    nx.draw_networkx_edges(subgraph, pos, alpha=0.5)
    nx.draw_networkx_labels(subgraph,pos,imgs_contained)

    
    # nx.draw(subgraph, with_labels=True,node_color=list(nodes_color.values()), pos=nx.spring_layout(G, k=0.8, iterations=20))
    markers = [plt.Line2D([0,0],[0,0], color=color, marker='o', linestyle='') for color in colors]
    clss_lists['mixed_images'] = 'darkgray'
    plt.legend(markers, clss_lists.keys(), numpoints=1)
    # plt.margins(x=0.09)
    # plt.legend()
    plt.savefig('cluster-images-'+check_iscc_or_hash+'-r'+str(r)+'.png')


if __name__ == "__main__":
    r=8
    check_iscc_or_hash = 'hash'
    rootdir = '../../photos/'
    hypercube = json.load(open('../r-'+str(r)+'/'+check_iscc_or_hash+'/hypercube.json', 'r'))

    #create_json_cids(rootdir, check_iscc_or_hash, r)
    objs = json.load(open('cls_imgs_cids_'+check_iscc_or_hash+'-r'+str(r)+'.json','r'))

    NODES = 2 ** r

    LABELS = {tuple(int(j) for j in create_binary_id(i, r))
            : create_binary_id(i, r) for i in range(0, NODES)}

    graph = nx.relabel_nodes(
            nx.generators.lattice.hypercube_graph(r), LABELS)

    plot_graph(graph, hypercube, objs, check_iscc_or_hash, r)




    

    