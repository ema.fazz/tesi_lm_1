import sys
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import json
import numpy as np


if __name__ == "__main__":

    gs = [2,4]

    for g in gs:

        fig_iscc = plt.figure(figsize=(12, 6))
        outer_iscc = gridspec.GridSpec(1, 3, wspace=0.2, hspace=0.2)

        res_iscc = json.load(open('results_query_json/results-iscc-pin-g'+str(g)+'.json','r'))
        res_hash = json.load(open('results_query_json/results-hash-pin-g'+str(g*4)+'.json','r'))

        # res_iscc = json.load(open('results-iscc-g'+str(g)+'.json','r'))
        # res_hash = json.load(open('results-hash-g'+str(g*4)+'.json','r'))
        i = 0

        find_max_iscc=[]
        find_max_hash=[]

        for k in res_iscc.keys():
            pos_obj_iscc = res_iscc[k]['positive_objs']
            all_obj_iscc = res_iscc[k]['all_objs']
            tot_hops_iscc = res_iscc[k]['total_hops']
            find_max_iscc.extend([pos_obj_iscc,all_obj_iscc,tot_hops_iscc])

        max_iscc = np.max(find_max_iscc)

        

        for k in res_iscc.keys():

            pos_obj_iscc = res_iscc[k]['positive_objs']
            all_obj_iscc = res_iscc[k]['all_objs']
            tot_hops_iscc = res_iscc[k]['total_hops']

            labels = list([k])

            x = np.arange(len(labels))
            width = 0.4
            
            ax_iscc = plt.subplot(outer_iscc[0,i])
            
            rects11 = ax_iscc.bar(x - width/2, [pos_obj_iscc], width/2, label='Positive')
            ax_iscc.text(0-width/2, pos_obj_iscc, pos_obj_iscc)
            rects12 = ax_iscc.bar(x, [all_obj_iscc], width/2, label='Total')
            ax_iscc.text(0, all_obj_iscc, all_obj_iscc)
            rects13 = ax_iscc.bar(x + width/2, [tot_hops_iscc], width/2, label='Hops')
            ax_iscc.text(0+width/2, tot_hops_iscc, tot_hops_iscc)
            
            ax_iscc.set_xticks(x, [k])
            ax_iscc.set_title(str(k))
            ax_iscc.axhline(y=0.0, color='r', linestyle='-')
            ax_iscc.legend()
            # ax_iscc.set_ylim(0,max_iscc+50)
            ax_iscc.set_ylim(0,max_iscc+3)
            
            fig_iscc.add_subplot(ax_iscc)

            i += 1

        ax_iscc_max = -sys.maxsize-1
        
        fig_iscc.tight_layout()

        # fig_iscc.savefig('./fig-iscc-'+str(g)+'.png')
        fig_iscc.savefig('./fig-iscc-pin-'+str(g)+'.png')

        i = 0
        fig_hash = plt.figure(figsize=(12, 6))
        outer_hash = gridspec.GridSpec(1, 3, wspace=0.2, hspace=0.2)


        for k in res_hash.keys():
            pos_obj_hash = res_hash[k]['positive_objs']
            all_obj_hash = res_hash[k]['all_objs']
            tot_hops_hash = res_hash[k]['total_hops']
            find_max_hash.extend([pos_obj_hash,all_obj_hash,tot_hops_hash])

        max_hash = np.max(find_max_hash)

        print(max_iscc)
        print(max_hash)

        for k in res_hash.keys():
            
            pos_obj_hash = res_hash[k]['positive_objs']
            all_obj_hash = res_hash[k]['all_objs']
            tot_hops_hash = res_hash[k]['total_hops']

            labels = list([k])

            x = np.arange(len(labels))
            width = 0.4
            
            ax_hash = plt.subplot(outer_hash[0,i])
            
            rects21 = ax_hash.bar(x - width/2, [pos_obj_hash], width/2, label='Positive')
            ax_hash.text(0-width/2, pos_obj_hash, pos_obj_hash)
            rects22 = ax_hash.bar(x, [all_obj_hash], width/2, label='Total')
            ax_hash.text(0, all_obj_hash, all_obj_hash)
            rects23 = ax_hash.bar(x + width/2, [tot_hops_hash], width/2, label='Hops')
            ax_hash.text(0+width/2, tot_hops_hash, tot_hops_hash)

            ax_hash.set_xticks(x, [k])
            ax_hash.set_title(str(k))
            ax_hash.axhline(y=0.0, color='r', linestyle='-')
            ax_hash.legend()
            # ax_hash.set_ylim(0,max_hash+200)
            ax_hash.set_ylim(0,max_hash+3)

            i += 1

        fig_hash.tight_layout()

        # fig_hash.savefig('./fig-hash-'+str(g*4)+'.png')
        fig_hash.savefig('./fig-hash-pin'+str(g*4)+'.png')
                















